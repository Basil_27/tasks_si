/*
//Bin_Print.cpp
//visual studio code
//Basil Sutormin, 2021
*/
#include <limits.h>
#include <stdio.h>

void Bin_Print(long num);

int main(const int argc,const char** argv){
    Bin_Print(0);
    Bin_Print(2);
    Bin_Print(8);
    Bin_Print(13);

    Bin_Print(-1024);
    Bin_Print(-15);
    Bin_Print(-1);

    printf("\nMax dec number is   %d: \n", LONG_MAX);
    Bin_Print(LONG_MAX);
    printf("Min dec number is  %d: \n", LONG_MIN);
    Bin_Print(LONG_MIN);
    
     return 0;
}

void Bin_Print(long num){
    if (num == 0)
    {
        printf("0\n");
    }
    else
    {
        unsigned long mask_bit = 1 << (sizeof(num) * 8 - 1);
        int skipZero = 1;

        while (mask_bit)
        {
            unsigned char ch_tmp = (num & mask_bit) ? 1 : 0;

            if (skipZero && ch_tmp)
                skipZero = 0;

            if (!skipZero)
                printf("%d", ch_tmp);

            mask_bit >>= 1;
        }
        printf("\n");
    }
}