/*
//main.cpp
//visual studio 2019
//Basil Sutormin, 2021
*/
#include "serialization.h"

int main(int argc, const char** argv) {
    List test1, test2;

    FILE* ptr_f = fopen("binary_list.dat", "wb");


    //test2.add_to_head("d_");
    test1.add_to_tail("When");
    test1.add_to_tail("I");
    test1.add_to_tail("find");
    test1.add_to_tail("myself");
    test1.add_to_tail("in times");
    test1.add_to_tail("of");
    test1.add_to_tail("trouble trouble trouble trouble");


    test1.Serialize(ptr_f);
    test1.print_List_data();
    fclose(ptr_f);


    FILE* ptr_f2 = fopen("binary_list.dat", "rb");
    test2.Deserialize(ptr_f2);
    test2.print_List_data();

    test2.print_all_Lists_data();
    fclose(ptr_f2);

    return 0;
}
