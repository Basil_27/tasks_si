﻿/*
// serialization.cpp
//visual studio 2019
//Basil Sutormin, 2021
*/
#include "serialization.h"

List::List() : head(nullptr),
tail(nullptr),
count(0)
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    generator = std::default_random_engine(seed);
}

List::~List() {
    while (head) {
        tail = head->next;
        delete head;
        head = tail;
    }
}
ListNode* List::set_Rand_ptr() {
    std::uniform_int_distribution<int> distribution(0, count);

    int rand_num = distribution(generator);
    int tmp_count = 1;
    if (rand_num == 0) {
        return nullptr;
    }
    else {
        for (ListNode* tmp_node = head; tmp_node; tmp_node = tmp_node->next, tmp_count++)
        {
            if (tmp_count == rand_num) {
                return tmp_node;
            }
        }
    }
}

void List::add_to_tail(string data_in) {
    ListNode* temp = new ListNode;
    temp->next = nullptr;
    temp->data = data_in;

    if (head != nullptr) {
        temp->prev = tail;
        tail->next = temp;
        tail = temp;
        temp->rand = set_Rand_ptr();

        count++;
    }
    else {
        temp->prev = nullptr;
        head = tail = temp;
        temp->rand = nullptr;
        count++;
    }
}

void List::print_List_data() {
    cout << "amount of nodes: " << count << endl;
    for (ListNode* tmp_node = head; tmp_node; tmp_node = tmp_node->next)
        cout << tmp_node->data << " ";
    cout << endl;
}

void List::print_all_Lists_data(){
    print_List_data();
    size_t i =0;
    for (ListNode* tmp_node = head; tmp_node; tmp_node = tmp_node->next,i++)

        cout <<"node index ["<<i <<"], prev ptr = "<< tmp_node->prev<<"\t"
             <<"node index ["<<i <<"], next ptr = "<< tmp_node->next<<"\t"
             <<"node index ["<<i <<"], rand ptr = "<< tmp_node->rand<<endl;
}


void List::Serialize(FILE* file_ptr) {
    if (file_ptr == nullptr) {
        printf("Write error\n");
    }
    else {
        fwrite(&count, sizeof(count), 1, file_ptr);
        unordered_map<ListNode*, size_t> ptrToId;
        ptrToId[nullptr] = 0;
        size_t idCount = 1;
        for (ListNode* tmp_node = head; tmp_node; tmp_node = tmp_node->next)
            ptrToId[tmp_node] = idCount++;

        for (ListNode* tmp_node = head; tmp_node; tmp_node = tmp_node->next)
        {
            size_t len = tmp_node->data.length();
            fwrite(reinterpret_cast<char*>(&len), sizeof(size_t), 1, file_ptr);
            fwrite(tmp_node->data.c_str(), tmp_node->data.length(), 1, file_ptr);
            fwrite(reinterpret_cast<char *>(&ptrToId[tmp_node->rand]), sizeof(ptrToId[tmp_node->rand]), 1, file_ptr);
        }
    }
}


void List::Deserialize(FILE* file_ptr) {

    
    if (file_ptr == nullptr) {

        printf("Write error\n");
    }
    else {
        size_t count_from_file;
        fread(&count_from_file, sizeof(count_from_file), 1, file_ptr);

        vector<ListNode*> IdToPtr(count_from_file + 1);
        IdToPtr[0] = nullptr;

        vector<size_t> randIds(count_from_file+1);

        for (size_t i = 1; i <= count_from_file; i++)
        {
            size_t len;
            fread(&len, sizeof(len), 1, file_ptr);
            char* tmp_str = new char[len];
            fread(tmp_str, sizeof(char)*len, 1, file_ptr);
            add_to_tail(string(tmp_str, len));
            delete[] tmp_str;
            fread(&randIds[i], sizeof(size_t), 1, file_ptr);
            IdToPtr[i] = head;
        }
        size_t i = 1;
        for (ListNode* tmp_node = head; tmp_node; tmp_node = tmp_node->next)
        {
            tmp_node->rand = IdToPtr[randIds[i++]];
        }
    }
}

