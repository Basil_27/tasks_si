﻿/*
// serialization.h
//visual studio 2019
//Basil Sutormin, 2021
*/
#pragma once
#include <iostream>
#include <cstdlib>
#include <unordered_map>
#include <string>
#include <cstdio>
#include <chrono>
#include <random>
#include <vector>
using std::unordered_map;
using std::vector;
using std::string;
using std::cout;
using std::endl;



struct ListNode {
    ListNode* prev;
    ListNode* next;
    ListNode* rand;

    string data;
};

class List {
public:
    List();
    ~List();


    void Serialize(FILE* file);
    void Deserialize(FILE* file);

    ListNode* set_Rand_ptr();

    void add_to_tail(string data_in);
    void print_List_data();
    void print_all_Lists_data();

private:
    ListNode* head;
    ListNode* tail;
    size_t count;

    std::default_random_engine generator;
};
