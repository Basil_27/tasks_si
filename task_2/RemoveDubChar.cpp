/*
//RemoveDubChar.cpp
//visual studio code
//Basil Sutormin, 2021
*/
#include <stdio.h>

int size_of(char *str);
void RemovedDups(char *str);


int main(){
    char data[] = {"AAA BBB AAA ddd cccd"};

    RemovedDups(data);

    printf("%s \n",data);

    return 0;
}


int size_of(char *str){ 
    int ch_size;
    for(ch_size=0; *str; str++)
        ch_size++;
    return ch_size;
}

void RemovedDups(char *str){
    int size = size_of(str);
    int tail=1,j,count=0;
    for (int i = 0; i < size; ++i) { 
        for (j = 0; j < tail; ++j){
            if (str[i] == str[i+1]) {
                count++;  
                break;
            } 
        }
        if (j == tail) {
            str[tail-1] = str[i];
            tail++;
        }
    }
    str[tail-1] = 0; 
}

